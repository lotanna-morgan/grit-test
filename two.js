function dec2bin(dec){

    var bin = (dec >>> 0).toString(2);
    if(bin.length != 32) {
        var binArr = bin.split("");
        var diff = 32 - bin.length;
        for(let i = 0; i < diff; i++) {
            binArr.unshift("0");
        }
        var newBin = binArr.toString().replace(/,/g, "");
        return newBin;
    }
    else if(bin.length == 32) {
        return bin;
    }
}

function reverseBinary(bin) {
    var newBinArr = bin.split("");
    return newBinArr.reverse().toString().replace(/,/g, "");
}

console.log( dec2bin(964176192) );
console.log( reverseBinary(dec2bin(964176192)) );